\documentclass[10pt]{article}

\usepackage{beton} % Concrete font
\DeclareFontSeriesDefault[rm]{bf}{sbc}
\usepackage[euler-digits,euler-hat-accent]{eulervm} % Euler math

\usepackage[margin=1.0in]{geometry}
\usepackage{titlesec,multicol}

% Need following hack so bibentry and hyperref play nicely
\usepackage{bibentry}
\makeatletter\let\saved@bibitem\@bibitem\makeatother
\usepackage{hyperref}
\makeatletter\let\@bibitem\saved@bibitem\makeatother

% Modify section spacing and fonts
\titlespacing*{\section}{0pt}{*0.5}{*0.5}
\titleformat*{\section}{\large\bfseries}
\titlespacing*{\subsection}{0pt}{*0.5}{*0.5}
\titleformat*{\subsection}{\normalsize\bfseries}

\renewcommand{\arraystretch}{1.3} % Give tables some breathing room

\begin{document}

% Set style for publications
\bibliographystyle{abbrv}
\nobibliography{Publications}

\noindent{\large\textbf{\uppercase{Craig Gross} }}\\
\noindent\rule{\textwidth}{1pt}

\section*{Contact and Other Information}
\begin{itemize}
	\item[] % Hacky way to indent everything over
	\begin{tabular}{r|p{4.4in}}
		Pronouns & he/him \\
		Email & \href{mailto:grosscra@msu.edu}{grosscra@msu.edu}\\
		GitHub & \url{https://github.com/cgross95}\\
		GitLab & \url{https://gitlab.com/grosscra}\\
		Work Address & Institute for Cyber-Enabled Research, Michigan State University \newline 567 Wilson Road \newline 1440 Biomedical \& Physical Sciences Building \newline East Lansing, MI 48824\\
	\end{tabular}
\end{itemize}

\hrule

\section*{Education}
\begin{itemize}
	\item[]
	\begin{tabular}{r|p{5.2in}}
		May 2023 & Ph.D. Applied Mathematics, \newline Michigan State University, East Lansing, Advisor: Mark Iwen, \newline Thesis: Sparsity in the Spectrum: Sparse Fourier Transforms and Spectral Methods for Functions of Many Dimensions\\
		May 2017 & B.S. Mathematics \& Computer Science, summa cum laude,\newline University of Arizona, Tucson, Advisor: Andrew Gillette
	\end{tabular}
\end{itemize}

\hrule

\section*{Employment}%

\begin{itemize}
	\item[]
	\begin{tabular}{p{1.2in}|p{4.1in}}
		Jan 2023 -- Present &\textbf{Research Computing Facilitator},\newline Institute for Cyber-Enabled Research, Michigan State University
	\end{tabular}
\end{itemize}

\hrule

\section*{Previous Professional Experience}

\begin{itemize}
	\item[]
		\begin{tabular}{p{1.7in}|p{4.1in}}
		Fall 2019 -- Spring 2020,\newline Spring 2021 -- Summer 2021,\newline Spring 2022 -- Summer 2022 & \textbf{Research Assistant},\newline Department of Mathematics, Michigan State University\\
		Summer 2018 -- Fall 2019,\newline Fall 2020, Fall 2021, \newline Fall 2022 & \textbf{Teaching Assistant},\newline Department of Mathematics, Michigan State University\\
		Summer 2020& \textbf{Data Science Student Intern}, Lawrence Livermore National Laboratory.
		\begin{itemize}
			\item Investigation of vortex detection in large scale OpenFOAM fluid flow simulations in a high performance computing environment.
			\item Machine learning approaches for biometric matching with MRI data.
			\item Mini courses in graph theory, multivariate regression, deep learning, and computational design of experiments.
		\end{itemize}\\
	\end{tabular}
\end{itemize}
\begin{itemize}
	\item[]
		\begin{tabular}{p{1.7in}|p{4.1in}}
		Spring 2014 -- Fall 2016& \textbf{Undergraduate Research Assistant}, University of Arizona.
		\begin{itemize}
			\item Implementation of serendipity basis function generations for finite element method. Project received Excellence in Undergraduate Research for Department of Mathematics. 
		\end{itemize}\\
	\end{tabular}
\end{itemize}

\hrule

\section*{Professional Interests}
\begin{itemize}
	\item[] High performance computing, accessible computing, high dimensional function approximation, numerical partial differential equations.
\end{itemize}

\hrule

\section*{Publications}

\subsection*{Published journal articles}

\begin{itemize}
	\item \bibentry{SFTforPDE}
	\item \bibentry{SFTR1L2020}
	\item \bibentry{DetMR1L2020}
\end{itemize}

\subsection*{Undergraduate}
\begin{itemize}
	\item \bibentry{NumericalStudies2018}
\end{itemize}

\hrule

\section*{Talks}

\subsection*{Conference talks}

\begin{itemize}
	\item \emph{Sparsifying high-dimensional, multiscale Fourier spectral methods}. Contributed talk, Great Lakes SIAM 2022 Annual Meeting. Wayne State University, Detroit, Michigan, September, 2022.
	\item \emph{Sparsifying high-dimensional, multiscale Fourier spectral methods}. Contributed talk, Canadian Mathematical Society summer meeting. Memorial University of Newfoundland, St. John's, Newfoundland and Labrador, Canada, June, 2022.
	\item \emph{A walk around the torus: SFTs in many dimensions}. Contributed talk, Online International Conference on Computational Harmonic Analysis, Online, September, 2021.
\end{itemize}

\subsection*{Invited talks and funded visits}

\begin{itemize}
	\item \emph{Applications of compressive sensing and high-dimensional function approximation in uncertainty quantification}. Analysis research seminar, Technische Universität Chemnitz, Chemnitz, Germany, January, 2020.
\end{itemize}


\subsection*{Other talks}

\begin{itemize}
	\item Semester long graduate level lecture series on compressive sensing and PDE-based uncertainty quantification to prepare for comprehensive exam, Michigan State University, Fall 2019.
	\item Various seminar talks, Michigan State University, 2017 - 2022.
\end{itemize}

\hrule

\section*{Workshops, Conferences, and Professional Development}

\subsection*{Professional development}%

\begin{itemize}
	\item Virtual Residency Workshop. Online. June, 2023.
	\begin{itemize}
		\item Series of talks and panels concerning advanced computing facilitation.
	\end{itemize}
	\item CyberAmbassadors Training. Online. January -- April, 2023.
	\begin{itemize}
		\item Nine modules on communication, teamwork, and leadership in oriented towards cyberinfrastructure professionals.
		\item Completed Certificate Program.
	\end{itemize}
\end{itemize}


\subsection*{Workshops attended}

\begin{itemize}
	\item Data Science for Democracy. Johns Hopkins University, Baltimore, Maryland. June, 2022.
		\begin{itemize}
			\item Worked with Baltimore Transit Equity Coalition to demonstrate impact of potential improvements to public transit system for disadvantaged areas in the city.
			\item Final product available at: \url{https://arcg.is/0SePnT}.
		\end{itemize}
	\item AARMS CRG Scientific Machine Learning workshop. Memorial University of Newfoundland, St. John's, Newfoundland and Labrador, Canada. June, 2022.
	\item PDE-based uncertainty quantification. Argonne National Laboratory, Lemont, Illinois. May, 2019.
	\item Finite element method for eigenvalue problems. Michigan Technological University, Houghton, Michigan. July, 2016.
\end{itemize}

\subsection*{Conferences attended}

\begin{itemize}
	\item Practice and Experience in Advanced Research Computing (PEARC). Providence, Rhode Island. Providence, Rhode Island. July 2024.
	\item The International Conference for High Performance
Computing, Networking, Storage, and Analysis. Denver, Colorado. November, 2023.
	\item Great Lakes SIAM 2022 annual meeting. Wayne State University, Detroit, Michigan. September, 2022.
	\item Canadian Mathematical Society summer meeting. Memorial University of Newfoundland, St. John's, Newfoundland and Labrador, Canada. June, 2022.
	\item paraDIGMS (Diversity in Graduate Mathematical Sciences). Online. April, 2020 and 2022.
	\item Online International Conference on Computational Harmonic Analysis. Online. September, 2021.
	\item Concentration week in randomness and determinism in compressive data acquisition. Texas A\&M, College Station, Texas. July, 2019.
\end{itemize}

\hrule

\section*{Teaching Experience}

\subsection*{Michigan State University, Institute for Cyber-Enabled Research}%
\label{sub:michigan_state_university_institute_for_cyber_enabled_research}

\begin{itemize}
	\item Workshops taught\newline
		\begin{tabular}{p{1in}|p{4in}}
			September 2023,\newline February 2024 & R for the HPCC				\begin{itemize}
				\item Developed and delivered for R users of ICER's HPCC
			\end{itemize}\\
			May 2023,\newline May 2024 & Introduction to R
						\begin{itemize}
							\item Developed and delivered for participants of the Advanced Computational Research Experience REU
						\end{itemize}
		\end{tabular}
\end{itemize}



\subsection*{Carpentries}
\label{sub:carpentries}

\begin{itemize}
	\item Certified Carpentries instructor
		\begin{itemize}
			\item Trained in delivering Software, Data, and Library Carpentries curricula via live coding and active learning strategies with emphasis on managing cognitive load, inclusive practices, and formative assessment.
			\item Contributed revisions to ``R for Social Sciences'' curriculum.
		\end{itemize}
	\item Workshops taught\newline
	\begin{tabular}{p{0.9in}|p{4.9in}}
		February 2024 & Introduction to Unix Shell, Using Git, and Programming with Python.\newline
		Michigan State University, in person. \newline Organized and hosted in collaboration with Michigan State University's Digital Humanities program and Digital Scholarship Lab.\\
		October 2022 & Introduction to Unix Shell, Using Git, and Programming with Python.\newline
		Carnegie Mellon University, online.\\
	\end{tabular}
\end{itemize}


\subsection*{Michigan State University, Department of Mathematics}
\label{sub:MSU}

\begin{itemize}
	\item Teaching assistant\newline
	\begin{tabular}{p{0.9in}|p{4.9in}}
		Fall 2022 & Matrix Algebra with Computational Applications (Instructor of record)\\
		Fall 2021 & Standards based graded college algebra (Instructor of record)\\
		Fall 2020 & Calculus II (Recitation instructor)\\
		Fall 2019 & Differential equations (Recitation instructor) \\
		Spring 2019 & Differential equations (Recitation instructor)\\
		Fall 2018 & Survey of calculus (Instructor of record) \\
		Summer 2018 & Calculus I (Exam writing, live-streaming review sessions, compiling supplemental instructional material)\\
		When teaching & Math learning center tutor
	\end{tabular}
	\item Other projects \newline
	\begin{tabular}{p{0.9in}|p{4.9in}}
		Summer 2019 & MATLAB-based Calculus II labs.
		\begin{itemize}
			\item Producing screen-cast video introductions and recaps of MATLAB-based programming labs for application focused sections of Calculus II.
		\end{itemize}
		\vspace{-0.3in}
	\end{tabular}
	\item Professional development\newline
	\begin{tabular}{p{0.9in}|p{4.9in}}
		Spring 2022 & Facilitating Accessible and Inclusive Mathematics Learning Environments
		\begin{itemize}
			\item Semester-long workshop pilot run by Program in Mathematics Education at MSU.
			\item Covered topics such as building student relationships and safe spaces for anti-deficit learning environments, student supports, legal theory and practice of mathematics accommodations, and universal design.
		\end{itemize}
		\vspace{-0.2in}\\
		2017 -- 2018
		& Introductory teaching workshop from MSU Center for Instructional Mentoring.
		\begin{itemize}
			\item Covered topics such as inclusive teaching, formative assessment, active learning, effective group work strategies, and lesson planning.
		\end{itemize}
		\vspace{-0.3in}
	\end{tabular}
\end{itemize}

\subsection*{University of Arizona}

\begin{itemize}
	\item Undergraduate teaching assistant\newline
	\begin{tabular}{lr|p{4in}}
		Spring & 2014 & Calculus I
	\end{tabular}
\end{itemize}

\hrule

\section*{Scholarships and Awards}

\begin{itemize}
	\item[] 
	\begin{tabular}{r|p{5in}}
		2022 & TA Award for Excellence in Teaching in the Department of Mathematics, Michigan State University (\$400)\\
		2022 & Douglass A.\ Spragg Endowed Fellowship, Michigan State University (\$1,800)\\
		2020 & Dr.\ Paul and Wilma Dressel Endowed Scholarship, Michigan State University (\$1,900)\\
		2017 & Department of mathematics recruiting fellowship, Michigan State University (\$8,000) \\
		2017 & Early start fellowship, Michigan State University (\$6,000)\\
		2017 & Excellence in undergraduate research for Mathematics Department and finalist for College of Science, University of Arizona\\
		2013 -- 2017 & Arizona wildcat excellence scholarship, University of Arizona (\$40,000)\\
		2013 -- 2016 & Worth and Dot Howard Foundation Scholarship (\$4,000) \\
		2015 & Galileo Scholar, University of Arizona (\$1,000)
	\end{tabular}
\end{itemize}

\hrule

\section*{Service}

\subsection*{Mentoring}
\label{sub:mentoring}

\begin{itemize}
	\item Mentoring ICER interns
		\begin{itemize}
			\item Mentored Data Science Masters student in improving software installation workflows on ICER's high-performance computing cluster.
			\item Mentored MSU undergraduate student in reorganizing upgrade software tools to improve HPCC user quality of life.
			\item Mentored MSU undergraduate student in benchmarking R code paralellized in using different techniques.
			\item Mentored intern collaboration in developing AI interfaces on MSU's HPCC.
			\item Presented professional development trainings to all ICER interns on common tools and workflows.
		\end{itemize}
	\item Mentoring MSU undergraduate students
	\begin{itemize}
		\item Undergraduate research mentor: Led research group of two undergraduate students in researching data pre-processing techniques with applications to social vulnerability datasets.
		\item Advanced track student mentor: Led mentoring pod of five undergraduate mathematics students new to MSU.
		\item Project mentor: Collaborated with and supervised undergraduate assistant in producing programming resources for MATLAB-based Calculus II labs.
	\end{itemize}
	\item Teaching 
	\begin{itemize}
		\item Lead TA, Calculus II: Observing, holding meetings with, and mentoring new teaching assistants and assisting in implementation of MATLAB-based programming labs. 
		\item Teaching mentor to first year graduate students
	\end{itemize}
\end{itemize}

\subsection*{Seminars}
\label{sub:Seminars}

\begin{itemize}
	\item \textbf{Organizer}, Programming in Practice Learning Community, Fall 2023 -- Spring 2024.
	\begin{itemize}
		\item Facilitated book club to learn introductory research software engineering skills.
	\end{itemize}
	\item \textbf{Organizer}, MSU Student Applied Math Seminar, Spring 2021 -- Spring 2023.
\end{itemize}

\subsection*{Organization membership}
\label{sub:organization_membership}

\begin{itemize}
	\item Student organizations
	\begin{itemize}
		\item \textbf{President}, MSU AMS Graduate Student Chapter, Fall 2020 -- Spring 2021.
		\item \textbf{Member}, MSU AWM Student Chapter, Fall 2020 -- Spring 2023.
		\item \textbf{Member}, MSU AMS Graduate Student Chapter, Fall 2019 -- Spring 2023.
	\end{itemize}
	\item Professional organizations
		\begin{itemize}
			\item \textbf{Member}, Canadian Mathematical Society (CMS), Spring 2022 -- Spring 2023.
			\item \textbf{Member}, United States Research Software Engineer Association (USRSE), Fall 2021 -- Present.
			\item \textbf{Member}, American Mathematical Society (AMS), Fall 2017 -- Spring 2023.
			\item \textbf{Member}, Society for Industrial and Applied Mathematics (SIAM), Fall 2017 -- Spring 2023.
		\end{itemize}
\end{itemize}

\hrule
	
\section*{Computational Skills}

\begin{itemize}
	\item[]
	\begin{tabular}{p{2in}|p{3.8in}}
		Expertise & Tools \\ \hline
		Expert (use every day) & Git, \LaTeX, MATLAB, Python, Unix/Linux environments, HPCC environments, shell scripting.\\
		Proficient (use confidently when consulting documentation) & C/C++, , Mathematica, R.\\
		Familiar (have experience, but need extended refresh) & Java, OpenFOAM, OpenMP, MPI, Selenium.
	\end{tabular}
\end{itemize}


\end{document}
